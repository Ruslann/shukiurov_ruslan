$( document ).ready(function() {
    function save(){
        var saveList;
        saveList = list.innerHTML;
        localStorage.setItem('addList', saveList);
    }
    //добавить задачу по клику
    addProblem.addEventListener('click', newElement);
//добавить задачу по нажатию на enter
    document.querySelector('input').addEventListener('keydown', function(e) {
    if (e.keyCode === 13) {
    newElement();
    }
    });
    
//добавление элементов(задач)
function newElement() {
    var li = document.createElement('li');
    var inputValue = document.getElementById('tasksList').value;
    var t = document.createTextNode(inputValue);
    li.appendChild(t);
    if (inputValue == "") {
        alert("your problems");
    } 
    else {
        document.getElementById('addList').appendChild(li);
    }
    //очистить поле    
    document.getElementById('tasksList').value = "";
    //создание иконки мусорки для удаления элементов   
    
    var trash = document.createElement("i");
    trash.className = 'fas fa-trash-alt close';
    //мусорка в li    
    li.appendChild(trash);
    save();
    };
    //зачеркивание или удаление выполненых задач
    var list = document.querySelector('ul');
    list.addEventListener('click', function (ev) {
    //если клик по li применяем класс checked    
    if(ev.target.tagName === "LI") {
       ev.target.classList.toggle('checked');
        save();
    }    
//если click по i то удаляем элемент        
     else if(ev.target.tagName === "I") {
       var div = ev.target.parentNode;
       div.remove();
       save();
    }
    
}, false);
    if(localStorage.getItem('addList')){
        list.innerHTML = localStorage.getItem('addList');
    }
});

